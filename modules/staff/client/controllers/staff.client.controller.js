'use strict';

// Articles controller
angular.module('staff').controller('StaffController', ['$scope', '$stateParams', '$location', 'Authentication', 'Staff',
  function ($scope, $stateParams, $location, Authentication, Staff) {
    $scope.authentication = Authentication;

    // Create new City
    $scope.create = function () {
      // Create new Article object
      var staff = new Staff({
        title: this.title,
        content: this.content
      });

      // Redirect after save
      staff.$save(function (response) {
        $location.path('staff/' + response._id);

        // Clear form fields
        $scope.title = '';
        $scope.content = '';
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    // Remove existing Article
    $scope.remove = function (staff) {
      if (staff) {
        staff.$remove();

        for (var i in $scope.staffs) {
          if ($scope.staffs[i] === staff) {
            $scope.staffs.splice(i, 1);
          }
        }
      } else {
        $scope.staff.$remove(function () {
          $location.path('staff');
        });
      }
    };

    // Update existing Article
    $scope.update = function () {
      var staff = $scope.staff;

      staff.$update(function () {
        $location.path('staff/' + staff._id);
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    // Find a list of Articles
    $scope.find = function () {
      $scope.staffs = Staff.query();
    };

    // Find existing Article
    $scope.findOne = function () {
      $scope.staffs = Staff.get({
        staffId: $stateParams.staffId
      });
    };
  }
]);
