'use strict';

// Configuring the Articles module
angular.module('staff').run(['Menus',
  function (Menus) {
    // Add the articles dropdown item
    Menus.addMenuItem('topbar', {
      title: 'Staff',
      state: 'staff',
      type: 'dropdown'
    });

    // Add the dropdown list item
    Menus.addSubMenuItem('topbar', 'staff', {
      title: 'List Staff',
      state: 'staff.list'
    });

    // Add the dropdown create item
    Menus.addSubMenuItem('topbar', 'staff', {
      title: 'Create staff',
      state: 'staff.create'
    });
  }
]);
