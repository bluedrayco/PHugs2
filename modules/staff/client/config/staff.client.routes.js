'use strict';

// Setting up route
angular.module('staff').config(['$stateProvider',
  function ($stateProvider) {
    // Articles state routing
    $stateProvider
      .state('staff', {
        abstract: true,
        url: '/staff',
        template: '<ui-view/>',
        data: {
          roles: ['user', 'admin']
        }
      })
      .state('staff.list', {
        url: '',
        templateUrl: 'modules/staff/views/list-staff.client.view.html'
      })
      .state('staff.create', {
        url: '/create',
        templateUrl: 'modules/staff/views/create-staff.client.view.html'
      })
      .state('staff.view', {
        url: '/:staffId',
        templateUrl: 'modules/staff/views/view-staff.client.view.html'
      })
      .state('staff.edit', {
        url: '/:staffId/edit',
        templateUrl: 'modules/staff/views/edit-staff.client.view.html'
      });
  }
]);
