'use strict';

//Articles service used for communicating with the cities REST endpoints
angular.module('staff').factory('Staff', ['$resource',
  function ($resource) {
    return $resource('api/staff/:staffId', {
      staffId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);
