'use strict';

/**
 * Module dependencies.
 */
var staffPolicy = require('../policies/staff.server.policy'),
  staff = require('../controllers/staff.server.controller');

module.exports = function (app) {
  // Articles collection routes
  app.route('/api/staff').all(staffPolicy.isAllowed)
    .get(staff.list)
    .post(staff.create);

  // Single article routes
  app.route('/api/staff/:staffId').all(staffPolicy.isAllowed)
    .get(staff.read)
    .put(staff.update)
    .delete(staff.delete);
    
  //checar si esta correcta la ruta
  app.route('/api/staff/near/:staffId/:latitude/:longitude').all(staffPolicy.isAllowed)
    .get(staff.nearStaffById);
    
  // Finish by binding the article middleware
  app.param('staffId', staff.staffByID);
};
