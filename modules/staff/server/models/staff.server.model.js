'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
        Schema = mongoose.Schema;

/**
 * Article Schema
 */
var StaffSchema = new Schema({
    name: String,
    age: Number,
    address: [String],
    phones: [String],
    ranking: Number,
    genre: String,
    photo: String,
    skills: [String],
    hobbies: [String],
    latitude: Number,
    longitude: Number,
    coordinates: [Number, Number],
    active: Boolean,
    city: {
        type: Schema.ObjectId,
        ref: 'City'
    },
    debit_card: {
        bank: {
            type: Schema.ObjectId,
            ref: 'Bank'
        },
        account_number: String,
        username: String,
        password: String,
        secure_number: String,
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    user_account: {
        type: Schema.ObjectId,
        ref: 'User'
    },
});

module.exports = mongoose.model('Staff', StaffSchema);