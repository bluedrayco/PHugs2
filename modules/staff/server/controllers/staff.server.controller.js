'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Staff = mongoose.model('Staff'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create a staff
 */
exports.create = function (req, res) {
  var staff = new Staff(req.body);
  staff.user   = req.user;
  staff.coords = [req.body.latitude,req.body.longitude];
  
  staff.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(staff);
    }
  });
};

/**
 * Show the current staff
 */
exports.read = function (req, res) {
  res.json(req.staff);
};

/**
 * Update a article
 */
exports.update = function (req, res) {
    var staff = req.staff;

    staff.name           = req.body.name;
    staff.age            = req.body.age;
    staff.address        = req.body.address;
    staff.phones         = req.body.phones;
    staff.ranking        = req.body.ranking;
    staff.genre          = req.body.genre;
    staff.skills         = req.body.skills;
    staff.hobbies        = req.body.hobbies;
    staff.latitude       = req.body.latitude;
    staff.longitude      = req.body.longitude;
    staff.coords         = [req.body.latitude,req.body.longitude];
    staff.active         = req.body.active;
    staff.user           = req.user;
    staff.user_account   = req.body.user;
    staff.debit_card     = req.body.debit_card;
    
    staff.save(function (err) {
      if (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      } else {
        res.json(staff);
      }
    });
};

/**
 * Delete an article
 */
exports.delete = function (req, res) {
  var staff = req.staff;

  staff.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(staff);
    }
  });
};

/**
 * List of Articles
 */
exports.list = function (req, res) {
  Staff.find().sort('-created').populate('user', 'displayName').exec(function (err, staff) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(staff);
    }
  });
};

/**
 * Article middleware
 */
exports.staffByID = function (req, res, next, id) {

    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).send({
        message: 'Staff is invalid'
      });
    }

    Staff.findById(id).populate('user', 'displayName').exec(function (err, staff) {
      if (err) {
        return next(err);
      } else if (!staff) {
        return res.status(404).send({
          message: 'No staff with that identifier has been found'
        });
      }
      req.staff = staff;
      next();
    });
};

exports.nearStaffById = function (req , res){
    
};