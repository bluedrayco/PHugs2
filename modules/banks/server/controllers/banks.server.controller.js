'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
        mongoose = require('mongoose'),
        Bank = mongoose.model('Bank'),
        errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * @api {post} /banks Create a Bank document
 * @apiName PostBank
 * @apiGroup Bank
 * @apiVersion 0.1.0
 * @apiPermission admin
 * 
 * @apiSampleRequest http://localhost:3000/api/banks
 * 
 * @apiParam  {String} name       name's bank
 * @apiParam  {Time}   open_hour  hour that the bank opens with format "hh:mm"
 * @apiParam  {Time}   close_hour hour that the bank closes with format "hh:mm"
 * @apiParam  {String} api_key    key necesary for connect with the bank's api
 * @apiParam  {String} url        api bank's url for consumes his resources
 * @apiParam  {Number} commision  percentage commision that we need to give to the bank for each transaction
 * 
 * @apiSuccess (200) {String} name       name's bank
 * @apiSuccess (200) {Time}   open_hour  hour that the bank opens
 * @apiSuccess (200) {Time}   close_hour hour that the bank closes
 * @apiSuccess (200) {String} api_key    key necesary for connect with the bank's api
 * @apiSuccess (200) {String} url        api bank's url for consumes his resources
 * @apiSuccess (200) {Number} commision  percentage commision that we need to give to the bank for each transaction
 * @apiSuccess (200) {Object} user       user that created this document
 * 
 * @apiError (404) message         The bank cannot created in the database
 * @apiError (403) message         User is not authorized
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {
 *      "__v": 0,
 *      "user": {
 *          "_id": "55fcb991a1daba470ec057e0",
 *          "displayName": "Roberto leroy Monroy Ruiz",
 *          "provider": "local",
 *          "username": "tecno",
 *          "__v": 0,
 *          "resetPasswordExpires": "2015-10-04T01:42:57.872Z",
 *          "resetPasswordToken": "9836a64ab4b785de7e9443c8cba9b3e1555a7191",
 *          "created": "2015-09-19T01:25:37.853Z",
 *          "roles": [
 *              "admin"
 *          ],
 *          "profileImageURL": "modules/users/img/profile/default.png",
 *          "email": "bluedrayco@gmail.com",
 *          "lastName": "Monroy Ruiz",
 *          "firstName": "Roberto leroy"
 *      },
 *      "name": "Banamex",
 *      "open_hour": "10:00",
 *      "close_hour": "20:00",
 *      "api_key": "dasdasdasdasdsada",
 *      "url": "/banamex.com/api",
 *      "commision": 3.5,
 *      "_id": "561b0fa0a6f4f9f07908d83a"
 *    }
 *    
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 403 Forbidden
 *     {
 *       "message": "User is not authorized"
 *     }
 */
exports.create = function (req, res) {
    var bank = new Bank(req.body);
    bank.user = req.user;

    bank.save(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(bank);
        }
    });
};

/**
 * @api {get} /banks/:id Get a Bank knows his Id
 * @apiName GetBank
 * @apiGroup Bank
 * @apiVersion 0.1.0
 * @apiPermission none
 * 
 * @apiSampleRequest http://localhost:3000/api/banks/:id
 * 
 * @apiParam  {String} id Bank unique Id
 * 
 * @apiSuccess (200) {String} name       name's bank
 * @apiSuccess (200) {Time}   open_hour  hour that the bank opens
 * @apiSuccess (200) {Time}   close_hour hour that the bank closes
 * @apiSuccess (200) {String} api_key    key necesary for connect with the bank's api
 * @apiSuccess (200) {String} url        api bank's url for consumes his resources
 * @apiSuccess (200) {Number} commision  percentage commision that we need to give to the bank for each transaction
 * @apiSuccess (200) {Object} user       user that created this document
 *    
 * @apiError (400) message         Bank is invalid
 * @apiError (403) message         User is not authorized
 * @apiError (404) message         No bank with that identifier has been found
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {
 *      "__v": 0,
 *      "user": {
 *          "_id": "55fcb991a1daba470ec057e0",
 *          "displayName": "Roberto leroy Monroy Ruiz",
 *          "provider": "local",
 *          "username": "tecno",
 *          "__v": 0,
 *          "resetPasswordExpires": "2015-10-04T01:42:57.872Z",
 *          "resetPasswordToken": "9836a64ab4b785de7e9443c8cba9b3e1555a7191",
 *          "created": "2015-09-19T01:25:37.853Z",
 *          "roles": [
 *              "admin"
 *          ],
 *          "profileImageURL": "modules/users/img/profile/default.png",
 *          "email": "bluedrayco@gmail.com",
 *          "lastName": "Monroy Ruiz",
 *          "firstName": "Roberto leroy"
 *      },
 *      "name": "Banamex",
 *      "open_hour": "10:00",
 *      "close_hour": "20:00",
 *      "api_key": "dasdasdasdasdsada",
 *      "url": "/banamex.com/api",
 *      "commision": 3.5,
 *      "_id": "561b0fa0a6f4f9f07908d83a"
 *    }
 *    
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 403 Forbidden
 *     {
 *       "message": "User is not authorized"
 *     }
 *     
 */
exports.read = function (req, res) {
    res.json(req.bank);
};

/**
 * @api {put} /banks/:id Update a Bank document by his id
 * @apiName PutBank
 * @apiGroup Bank
 * @apiVersion 0.1.0
 * @apiPermission admin
 * 
 * @apiSampleRequest http://localhost:3000/api/banks/:id
 * 
 * @apiParam  {String} id Bank unique Id
 * 
 * @apiParam  {String} name       new name's bank
 * @apiParam  {Time}   open_hour  new hour that the bank opens with format "hh:mm"
 * @apiParam  {Time}   close_hour new hour that the bank closes with format "hh:mm"
 * @apiParam  {String} api_key    new key necesary for connect with the bank's api
 * @apiParam  {String} url        new api bank's url for consumes his resources
 * @apiParam  {Number} commision  new percentage commision that we need to give to the bank for each transaction
 * 
 * @apiSuccess (200) {String} name       name's bank
 * @apiSuccess (200) {Time}   open_hour  hour that the bank opens
 * @apiSuccess (200) {Time}   close_hour hour that the bank closes
 * @apiSuccess (200) {String} api_key    key necesary for connect with the bank's api
 * @apiSuccess (200) {String} url        api bank's url for consumes his resources
 * @apiSuccess (200) {Number} commision  percentage commision that we need to give to the bank for each transaction
 * @apiSuccess (200) {Object} user       user that created this document
 * 
 * @apiError (404) message         The bank cannot updated in the database
 * @apiError (403) message         User is not authorized
 * @apiError (404) message         No bank with that identifier has been found
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {
 *      "__v": 0,
 *      "user": {
 *          "_id": "55fcb991a1daba470ec057e0",
 *          "displayName": "Roberto leroy Monroy Ruiz",
 *          "provider": "local",
 *          "username": "tecno",
 *          "__v": 0,
 *          "resetPasswordExpires": "2015-10-04T01:42:57.872Z",
 *          "resetPasswordToken": "9836a64ab4b785de7e9443c8cba9b3e1555a7191",
 *          "created": "2015-09-19T01:25:37.853Z",
 *          "roles": [
 *              "admin"
 *          ],
 *          "profileImageURL": "modules/users/img/profile/default.png",
 *          "email": "bluedrayco@gmail.com",
 *          "lastName": "Monroy Ruiz",
 *          "firstName": "Roberto leroy"
 *      },
 *      "name": "Banamex",
 *      "open_hour": "10:00",
 *      "close_hour": "20:00",
 *      "api_key": "dasdasdasdasdsada",
 *      "url": "/banamex.com/api",
 *      "commision": 3.5,
 *      "_id": "561b0fa0a6f4f9f07908d83a"
 *    }
 *    
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 403 Forbidden
 *     {
 *       "message": "User is not authorized"
 *     }
 */
exports.update = function (req, res) {
    var bank = req.bank;

    bank.name = req.body.name;
    bank.open_hour = req.body.open_hour;
    bank.close_hour = req.body.close_hour;
    bank.api_key = req.body.api_key;
    bank.url = req.body.url;
    bank.commision = req.body.commision;

    bank.save(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(bank);
        }
    });
};

/**
 * @api {delete} /banks/:id Delete a Bank knows his Id
 * @apiName DeleteBank
 * @apiGroup Bank
 * @apiVersion 0.1.0
 * @apiPermission admin
 * 
 * @apiSampleRequest http://localhost:3000/api/banks/:id
 * 
 * @apiParam  {String} id Bank unique Id the will be deleted
 * 
 * @apiSuccess (200) {String} name       name's bank
 * @apiSuccess (200) {Time}   open_hour  hour that the bank opens
 * @apiSuccess (200) {Time}   close_hour hour that the bank closes
 * @apiSuccess (200) {String} api_key    key necesary for connect with the bank's api
 * @apiSuccess (200) {String} url        api bank's url for consumes his resources
 * @apiSuccess (200) {Number} commision  percentage commision that we need to give to the bank for each transaction
 * @apiSuccess (200) {Object} user       user that created this document
 *    
 * @apiError (400) message         Bank is invalid
 * @apiError (403) message         User is not authorized
 * @apiError (404) message         No bank with that identifier has been found
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {
 *      "__v": 0,
 *      "user": {
 *          "_id": "55fcb991a1daba470ec057e0",
 *          "displayName": "Roberto leroy Monroy Ruiz",
 *          "provider": "local",
 *          "username": "tecno",
 *          "__v": 0,
 *          "resetPasswordExpires": "2015-10-04T01:42:57.872Z",
 *          "resetPasswordToken": "9836a64ab4b785de7e9443c8cba9b3e1555a7191",
 *          "created": "2015-09-19T01:25:37.853Z",
 *          "roles": [
 *              "admin"
 *          ],
 *          "profileImageURL": "modules/users/img/profile/default.png",
 *          "email": "bluedrayco@gmail.com",
 *          "lastName": "Monroy Ruiz",
 *          "firstName": "Roberto leroy"
 *      },
 *      "name": "Banamex",
 *      "open_hour": "10:00",
 *      "close_hour": "20:00",
 *      "api_key": "dasdasdasdasdsada",
 *      "url": "/banamex.com/api",
 *      "commision": 3.5,
 *      "_id": "561b0fa0a6f4f9f07908d83a"
 *    }
 *    
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 403 Forbidden
 *     {
 *       "message": "User is not authorized"
 *     }
 *     
 */
exports.delete = function (req, res) {
    var bank = req.bank;

    bank.remove(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(bank);
        }
    });
};

/**
 * @api {get} /banks Get Banks (support query params)
 * @apiName GetBanks
 * @apiGroup Bank
 * @apiVersion 0.1.0
 * @apiPermission none
 * 
 * @apiSampleRequest http://localhost:3000/api/banks
 * 
 * 
 * @apiSuccess (200) {String} name       name's bank
 * @apiSuccess (200) {Time}   open_hour  hour that the bank opens
 * @apiSuccess (200) {Time}   close_hour hour that the bank closes
 * @apiSuccess (200) {String} api_key    key necesary for connect with the bank's api
 * @apiSuccess (200) {String} url        api bank's url for consumes his resources
 * @apiSuccess (200) {Number} commision  percentage commision that we need to give to the bank for each transaction
 * @apiSuccess (200) {Object} user       user that created this document
 *    
 * @apiError (403) message         User is not authorized
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    [{
 *      "__v": 0,
 *      "user": {
 *          "_id": "55fcb991a1daba470ec057e0",
 *          "displayName": "Roberto leroy Monroy Ruiz",
 *          "provider": "local",
 *          "username": "tecno",
 *          "__v": 0,
 *          "resetPasswordExpires": "2015-10-04T01:42:57.872Z",
 *          "resetPasswordToken": "9836a64ab4b785de7e9443c8cba9b3e1555a7191",
 *          "created": "2015-09-19T01:25:37.853Z",
 *          "roles": [
 *              "admin"
 *          ],
 *          "profileImageURL": "modules/users/img/profile/default.png",
 *          "email": "bluedrayco@gmail.com",
 *          "lastName": "Monroy Ruiz",
 *          "firstName": "Roberto leroy"
 *      },
 *      "name": "Banamex",
 *      "open_hour": "10:00",
 *      "close_hour": "20:00",
 *      "api_key": "dasdasdasdasdsada",
 *      "url": "/banamex.com/api",
 *      "commision": 3.5,
 *      "_id": "561b0fa0a6f4f9f07908d83a"
 *    }]
 *    
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 403 Forbidden
 *     {
 *       "message": "User is not authorized"
 *     }
 *     
 */
exports.list = function (req, res) {
    req.log.info('hola mundo...');
    console.log(req.log);
    req.log.info({res: res}, 'done response');
    if(req.query['{}']!== undefined ){
        delete req.query['{}'];
    }
    var query = req.query;
    
    console.log(query);
    Bank.find(query).sort('-created').populate('user', 'displayName').exec(function (err, banks) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(banks);
        }
    });
};

/**
 * Article middleware
 */
exports.bankByID = function (req, res, next, id) {

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).send({
            message: 'Bank is invalid'
        });
    }

    Bank.findById(id).populate('user', 'displayName').exec(function (err, bank) {
        if (err) {
            return next(err);
        } else if (!bank) {
            return res.status(404).send({
                message: 'No bank with that identifier has been found'
            });
        }
        req.bank = bank;
        next();
    });
};
