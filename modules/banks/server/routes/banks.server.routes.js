'use strict';

/**
 * Module dependencies.
 */
var banksPolicy = require('../policies/banks.server.policy'),
        banks = require('../controllers/banks.server.controller');

module.exports = function (app) {
    // Articles collection routes
    app.route('/api/banks').all(banksPolicy.isAllowed)
            .get(banks.list)
            .post(banks.create);

    // Single article routes
    app.route('/api/banks/:bankId').all(banksPolicy.isAllowed)
            .get(banks.read)
            .put(banks.update)
            .delete(banks.delete);

    // Finish by binding the article middleware
    app.param('bankId', banks.bankByID);
};
