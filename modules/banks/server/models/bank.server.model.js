'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
        Schema = mongoose.Schema;

/**
 * Article Schema
 */
var BankSchema = new Schema({
    name: String,
    open_hour: String,
    close_hour: String,
    api_key: String,
    url: String,
    commision: Number,
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

mongoose.model('Bank', BankSchema);