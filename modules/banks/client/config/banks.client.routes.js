'use strict';

// Setting up route
angular.module('banks').config(['$stateProvider',
  function ($stateProvider) {
    // Articles state routing
    $stateProvider
      .state('banks', {
        abstract: true,
        url: '/banks',
        template: '<ui-view/>',
        data: {
          roles: ['user', 'admin']
        }
      })
      .state('banks.list', {
        url: '',
        templateUrl: 'modules/banks/views/list-banks.client.view.html'
      })
      .state('banks.create', {
        url: '/create',
        templateUrl: 'modules/banks/views/create-bank.client.view.html'
      })
      .state('banks.view', {
        url: '/:bankId',
        templateUrl: 'modules/banks/views/view-bank.client.view.html'
      })
      .state('banks.edit', {
        url: '/:bankId/edit',
        templateUrl: 'modules/banks/views/edit-bank.client.view.html'
      });
  }
]);
