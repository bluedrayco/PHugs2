'use strict';

// Configuring the Articles module
angular.module('banks').run(['Menus',
  function (Menus) {
    // Add the articles dropdown item
    Menus.addMenuItem('topbar', {
      title: 'Banks',
      state: 'banks',
      type: 'dropdown'
    });

    // Add the dropdown list item
    Menus.addSubMenuItem('topbar', 'banks', {
      title: 'List Banks',
      state: 'banks.list'
    });

    // Add the dropdown create item
    Menus.addSubMenuItem('topbar', 'banks', {
      title: 'Create banks',
      state: 'banks.create'
    });
  }
]);
