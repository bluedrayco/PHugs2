'use strict';

//Articles service used for communicating with the cities REST endpoints
angular.module('banks').factory('Banks', ['$resource',
    function ($resource) {
        return $resource('api/banks/:bankId', {
            bankId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);
