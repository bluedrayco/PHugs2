'use strict';

// Articles controller
angular.module('banks').controller('BanksController', ['$scope', '$stateParams', '$location', 'Authentication', 'Banks',
    function ($scope, $stateParams, $location, Authentication, Banks) {
        $scope.authentication = Authentication;

        $scope.create = function () {
            // Create new Article object
            var bank = new Banks({
                title: this.title,
                content: this.content
            });

            // Redirect after save
            bank.$save(function (response) {
                $location.path('banks/' + response._id);

                // Clear form fields
                $scope.title = '';
                $scope.content = '';
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Remove existing Article
        $scope.remove = function (bank) {
            if (bank) {
                bank.$remove();

                for (var i in $scope.banks) {
                    if ($scope.banks[i] === bank) {
                        $scope.banks.splice(i, 1);
                    }
                }
            } else {
                $scope.banks.$remove(function () {
                    $location.path('banks');
                });
            }
        };

        // Update existing Article
        $scope.update = function () {
            var bank = $scope.bank;

            bank.$update(function () {
                $location.path('banks/' + bank._id);
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Find a list of Articles
        $scope.find = function () {
            $scope.banks = Banks.query();
        };

        // Find existing Article
        $scope.findOne = function () {
            $scope.banks = Banks.get({
                bankId: $stateParams.bankId
            });
        };
    }
]);
