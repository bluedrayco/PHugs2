'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  City = mongoose.model('City'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * @api {post} /cities Create a City document
 * @apiName PostCity
 * @apiGroup City
 * @apiVersion 0.1.0
 * @apiPermission admin
 * 
 * @apiSampleRequest http://localhost:3000/api/cities
 * 
 * @apiParam  {String} name    names'city
 * @apiParam  {String} state   states'city
 * @apiParam  {String} country country's city
 * 
 * @apiSuccess (200) {String} name    names' new city
 * @apiSuccess (200) {Time}   state   states' new city
 * @apiSuccess (200) {Time}   country country's new city
 * @apiSuccess (200) {Object} user    user that created this document
 * 
 * @apiError (404) message         The city cannot created in the database
 * @apiError (403) message         User is not authorized
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "__v": 0,
 *       "user": {
 *           "_id": "55fcb991a1daba470ec057e0",
 *           "displayName": "Roberto leroy Monroy Ruiz",
 *           "provider": "local",
 *           "username": "tecno",
 *           "__v": 0,
 *           "resetPasswordExpires": "2015-10-04T01:42:57.872Z",
 *           "resetPasswordToken": "9836a64ab4b785de7e9443c8cba9b3e1555a7191",
 *           "created": "2015-09-19T01:25:37.853Z",
 *           "roles": [
 *               "admin"
 *           ],
 *           "profileImageURL": "modules/users/img/profile/default.png",
 *           "email": "bluedrayco@gmail.com",
 *           "lastName": "Monroy Ruiz",
 *           "firstName": "Roberto leroy"
 *       },
 *       "name": "Mexico",
 *       "country": "DF",
 *       "state": "DF",
 *       "_id": "564a64278690563f445d1b50"
 *    }
 *    
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 403 Forbidden
 *     {
 *       "message": "User is not authorized"
 *     }
 */
exports.create = function (req, res) {
  var city = new City(req.body);
  city.user = req.user;

  city.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(city);
    }
  });
};

/**
    * @api {get} /cities/:id Get a City knows his Id
 * @apiName GetCity
 * @apiGroup City
 * @apiVersion 0.1.0
 * @apiPermission none
 * 
 * @apiSampleRequest http://localhost:3000/api/banks/:id
 * 
 * @apiParam  {String} id Bank unique Id
 * 
 * @apiSuccess (200) {String} name    names' new city
 * @apiSuccess (200) {Time}   state   states' new city
 * @apiSuccess (200) {Time}   country country's new city
 * @apiSuccess (200) {Object} user    user that created this document
 *    
 * @apiError (400) message         City is invalid
 * @apiError (403) message         User is not authorized
 * @apiError (404) message         No bank with that identifier has been found
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {
 *      "__v": 0,
 *      "user": {
 *          "_id": "55fcb991a1daba470ec057e0",
 *          "displayName": "Roberto leroy Monroy Ruiz",
 *          "provider": "local",
 *          "username": "tecno",
 *          "__v": 0,
 *          "resetPasswordExpires": "2015-10-04T01:42:57.872Z",
 *          "resetPasswordToken": "9836a64ab4b785de7e9443c8cba9b3e1555a7191",
 *          "created": "2015-09-19T01:25:37.853Z",
 *          "roles": [
 *              "admin"
 *          ],
 *          "profileImageURL": "modules/users/img/profile/default.png",
 *          "email": "bluedrayco@gmail.com",
 *          "lastName": "Monroy Ruiz",
 *          "firstName": "Roberto leroy"
 *      },
 *      "name": "Banamex",
 *      "open_hour": "10:00",
 *      "close_hour": "20:00",
 *      "api_key": "dasdasdasdasdsada",
 *      "url": "/banamex.com/api",
 *      "commision": 3.5,
 *      "_id": "561b0fa0a6f4f9f07908d83a"
 *    }
 *    
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 403 Forbidden
 *     {
 *       "message": "User is not authorized"
 *     }
 *     
 */
exports.read = function (req, res) {
  res.json(req.city);
};

/**
 * Update a article
 */
exports.update = function (req, res) {
  var city = req.city;

  city.title   = req.body.title;
  city.state   = req.body.state;
  city.country = req.body.country;

  city.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(city);
    }
  });
};

/**
 * Delete an article
 */
exports.delete = function (req, res) {
  var city = req.city;

  city.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(city);
    }
  });
};

/**
 * List of Articles
 */
exports.list = function (req, res) {
  City.find().sort('-created').populate('user', 'displayName').exec(function (err, cities) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(cities);
    }
  });
};

/**
 * Article middleware
 */
exports.cityByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'City is invalid'
    });
  }

  City.findById(id).populate('user', 'displayName').exec(function (err, city) {
    if (err) {
      return next(err);
    } else if (!city) {
      return res.status(404).send({
        message: 'No city with that identifier has been found'
      });
    }
    req.city = city;
    next();
  });
};
