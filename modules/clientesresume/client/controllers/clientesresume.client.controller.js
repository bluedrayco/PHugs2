'use strict';

// Articles controller
angular.module('clientesresume').controller('ClientesResumeController', ['$scope', '$stateParams', '$location', 'Authentication', 'ClientesResume',
  function ($scope, $stateParams, $location, Authentication, ClientesResume) {
    $scope.authentication = Authentication;

    // Create new City
    $scope.create = function () {
      // Create new Article object
      var clientesresume = new ClientesResume({
        title: this.title,
        content: this.content
      });

      // Redirect after save
      clientesresume.$save(function (response) {
        $location.path('clientesresume/' + response._id);

        // Clear form fields
        $scope.title = '';
        $scope.content = '';
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    // Remove existing Article
    $scope.remove = function (clienteresume) {
      if (clienteresume) {
        clienteresume.$remove();

        for (var i in $scope.clientesresume) {
          if ($scope.clientesresume[i] === clienteresume) {
            $scope.clientesresume.splice(i, 1);
          }
        }
      } else {
        $scope.clienteresume.$remove(function () {
          $location.path('clientesresume');
        });
      }
    };

    // Update existing Article
    $scope.update = function () {
      var clienteresume = $scope.clienteresume;

      clienteresume.$update(function () {
        $location.path('clientesresume/' + clienteresume._id);
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    // Find a list of Articles
    $scope.find = function () {
      $scope.clientesresume = ClientesResume.query();
    };

    // Find existing Article
    $scope.findOne = function () {
      $scope.clientesresume = ClientesResume.get({
        clienteresumeId: $stateParams.clienteresumeId
      });
    };
  }
]);
