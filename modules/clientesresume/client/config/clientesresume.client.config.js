'use strict';

// Configuring the Articles module
angular.module('clientesresume').run(['Menus',
  function (Menus) {
    // Add the articles dropdown item
    Menus.addMenuItem('topbar', {
      title: 'ClientesResume',
      state: 'clientesresume',
      type: 'dropdown'
    });

    // Add the dropdown list item
    Menus.addSubMenuItem('topbar', 'clientesresume', {
      title: 'List Clientes Resume',
      state: 'clientesresume.list'
    });

    // Add the dropdown create item
    Menus.addSubMenuItem('topbar', 'clientesresume', {
      title: 'Create cliente resume',
      state: 'clientesresume.create'
    });
  }
]);
