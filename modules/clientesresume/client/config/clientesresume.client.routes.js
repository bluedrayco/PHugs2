'use strict';

// Setting up route
angular.module('clientesresume').config(['$stateProvider',
  function ($stateProvider) {
    // Articles state routing
    $stateProvider
      .state('clientesresume', {
        abstract: true,
        url: '/clientesresume',
        template: '<ui-view/>',
        data: {
          roles: ['user', 'admin']
        }
      })
      .state('clientesresume.list', {
        url: '',
        templateUrl: 'modules/clientesresume/views/list-clientesresume.client.view.html'
      })
      .state('clientesresume.create', {
        url: '/create',
        templateUrl: 'modules/clientesresume/views/create-clienteresume.client.view.html'
      })
      .state('clientesresume.view', {
        url: '/:clienteresumeId',
        templateUrl: 'modules/clientesresume/views/view-clienteresume.client.view.html'
      })
      .state('clientesresume.edit', {
        url: '/:clienteresumeId/edit',
        templateUrl: 'modules/clientesresume/views/edit-clienteresume.client.view.html'
      });
  }
]);
