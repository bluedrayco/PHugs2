'use strict';

//Articles service used for communicating with the cities REST endpoints
angular.module('clientesresume').factory('ClientesResume', ['$resource',
  function ($resource) {
    return $resource('api/clientesresume/:clienteresumeId', {
      clienteresumeId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);
