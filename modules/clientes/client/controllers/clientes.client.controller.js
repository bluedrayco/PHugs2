'use strict';

// Articles controller
angular.module('clientes').controller('ClientsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Clientes',
  function ($scope, $stateParams, $location, Authentication, Clientes) {
    $scope.authentication = Authentication;

    // Create new City
    $scope.create = function () {
      // Create new Article object
      var client = new Clientes({
        title: this.title,
        content: this.content
      });

      // Redirect after save
      client.$save(function (response) {
        $location.path('clientes/' + response._id);

        // Clear form fields
        $scope.title = '';
        $scope.content = '';
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    // Remove existing Article
    $scope.remove = function (client) {
      if (client) {
        client.$remove();

        for (var i in $scope.clients) {
          if ($scope.clients[i] === client) {
            $scope.clients.splice(i, 1);
          }
        }
      } else {
        $scope.client.$remove(function () {
          $location.path('clientes');
        });
      }
    };

    // Update existing Article
    $scope.update = function () {
      var client = $scope.client;

      client.$update(function () {
        $location.path('clientes/' + client._id);
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    // Find a list of Articles
    $scope.find = function () {
      $scope.clients = Clientes.query();
    };

    // Find existing Article
    $scope.findOne = function () {
      $scope.clients = Clientes.get({
        clientId: $stateParams.clientId
      });
    };
  }
]);
