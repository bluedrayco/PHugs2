'use strict';

// Setting up route
angular.module('clientes').config(['$stateProvider',
  function ($stateProvider) {
    // Articles state routing
    $stateProvider
      .state('clientes', {
        abstract: true,
        url: '/clientes',
        template: '<ui-view/>',
        data: {
          roles: ['user', 'admin']
        }
      })
      .state('clientes.list', {
        url: '',
        templateUrl: 'modules/clientes/views/list-clientes.client.view.html'
      })
      .state('clientes.create', {
        url: '/create',
        templateUrl: 'modules/clientes/views/create-cliente.client.view.html'
      })
      .state('clientes.view', {
        url: '/:clientId',
        templateUrl: 'modules/clientes/views/view-cliente.client.view.html'
      })
      .state('clientes.edit', {
        url: '/:clientId/edit',
        templateUrl: 'modules/clientes/views/edit-cliente.client.view.html'
      });
  }
]);
