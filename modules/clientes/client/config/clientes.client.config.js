'use strict';

// Configuring the Articles module
angular.module('clientes').run(['Menus',
  function (Menus) {
    // Add the articles dropdown item
    Menus.addMenuItem('topbar', {
      title: 'Clientes',
      state: 'clientes',
      type: 'dropdown'
    });

    // Add the dropdown list item
    Menus.addSubMenuItem('topbar', 'clientes', {
      title: 'List Clients',
      state: 'clientes.list'
    });

    // Add the dropdown create item
    Menus.addSubMenuItem('topbar', 'clientes', {
      title: 'Create clientes',
      state: 'clientes.create'
    });
  }
]);
