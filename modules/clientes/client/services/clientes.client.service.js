'use strict';

//Articles service used for communicating with the cities REST endpoints
angular.module('clientes').factory('Clientes', ['$resource',
  function ($resource) {
    return $resource('api/clientes/:clientId', {
      clientId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);
