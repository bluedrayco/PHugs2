'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
        Schema = mongoose.Schema;

/**
 * 
 * Client Schema
 */
var ClientSchema = new Schema({
    name: String,
    city: {
        type: Schema.ObjectId,
        ref: 'City'
    },
    age: Number,
    credit_cards: [{
            bank: {
                type: Schema.ObjectId,
                ref: 'Bank'
            },
            account_number: String,
            username: String,
            password: String,
            secure_number: String,
        }],
    genred: String,
    skills: [String],
    hobbies: [String],
    config:[{param:String,value:String}],
    active: Boolean,
    latitude: Number,
    longitude: Number,
    coordinates: [Number, Number],
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    user_account: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

mongoose.model('Client', ClientSchema);