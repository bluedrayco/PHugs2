'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Client = mongoose.model('Client'),
  City = mongoose.model('City'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));
  

/**
 * Create a article
 */
exports.create = function (req, res) {
    var client = new Client(req.body);
    client.user = req.user;
    client.save(function (err) {
        if (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        } else {
          res.json(client);
        }
    });
};

/**
 * Show the current article
 */
exports.read = function (req, res) {
  res.json(req.client);
};

/**
 * Update a article
 */
exports.update = function (req, res) {
    var client = req.client;

    client.name           = req.body.name;
    client.city           = req.body.city;
    client.age            = req.body.age;
    client.credit_cards   = req.body.credit_cards;
    client.genred         = req.body.genred;
    client.skills         = req.body.skills;
    client.hobbies        = req.body.hobbies;
    client.active         = req.body.active;
    client.latitude       = req.body.latitude;
    client.longitude      = req.body.longitude;
    client.coords         = [req.body.latitude,req.body.longitude];
    client.user_account   = req.body.user_account;

    client.save(function (err) {
        if (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        } else {
          res.json(client);
        }
    });
};

/**
 * Delete an article
 */
exports.delete = function (req, res) {
  var client = req.client;

  client.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(client);
    }
  });
};

/**
 * List of Articles
 */
exports.list = function (req, res) {
  Client.find().sort('-created').populate('user', 'displayName').populate('credit_cards').populate('city').exec(function (err, clients) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(clients);
    }
  });
};

/**
 * Article middleware
 */
exports.clientByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Client is invalid'
    });
  }

  Client.findById(id).populate('user', 'displayName').exec(function (err, client) {
    if (err) {
      return next(err);
    } else if (!client) {
      return res.status(404).send({
        message: 'No client with that identifier has been found'
      });
    }
    req.client = client;
    next();
  });
};
