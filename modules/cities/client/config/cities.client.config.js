'use strict';

// Configuring the Articles module
angular.module('cities').run(['Menus',
  function (Menus) {
    // Add the articles dropdown item
    Menus.addMenuItem('topbar', {
      title: 'Cities',
      state: 'cities',
      type: 'dropdown'
    });

    // Add the dropdown list item
    Menus.addSubMenuItem('topbar', 'cities', {
      title: 'List Cities',
      state: 'cities.list'
    });

    // Add the dropdown create item
    Menus.addSubMenuItem('topbar', 'cities', {
      title: 'Create cities',
      state: 'cities.create'
    });
  }
]);
