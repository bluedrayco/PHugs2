'use strict';

// Setting up route
angular.module('cities').config(['$stateProvider',
  function ($stateProvider) {
    // Articles state routing
    $stateProvider
      .state('cities', {
        abstract: true,
        url: '/cities',
        template: '<ui-view/>',
        data: {
          roles: ['user', 'admin']
        }
      })
      .state('cities.list', {
        url: '',
        templateUrl: 'modules/cities/views/list-cities.client.view.html'
      })
      .state('cities.create', {
        url: '/create',
        templateUrl: 'modules/cities/views/create-city.client.view.html'
      })
      .state('cities.view', {
        url: '/:cityId',
        templateUrl: 'modules/cities/views/view-city.client.view.html'
      })
      .state('cities.edit', {
        url: '/:cityId/edit',
        templateUrl: 'modules/cities/views/edit-city.client.view.html'
      });
  }
]);
