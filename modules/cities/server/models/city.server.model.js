'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Article Schema
 */
var CitySchema = new Schema({
    name      : String,
    state     : String,
    country   : String,
    user: {
        type: Schema.ObjectId,
        ref: 'User'
  }
});

mongoose.model('City', CitySchema);