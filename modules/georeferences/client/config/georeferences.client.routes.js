'use strict';

// Setting up route
angular.module('georeferences').config(['$stateProvider',
  function ($stateProvider) {
    // Articles state routing
    $stateProvider
      .state('georeferences', {
        abstract: true,
        url: '/georeferences',
        template: '<ui-view/>',
        data: {
          roles: ['user', 'admin']
        }
      })
      .state('georeferences.list', {
        url: '',
        templateUrl: 'modules/georeferences/views/list-georeferences.client.view.html'
      })
      .state('georeferences.create', {
        url: '/create',
        templateUrl: 'modules/georeferences/views/create-georeference.client.view.html'
      })
      .state('georeferences.view', {
        url: '/:georeferenceId',
        templateUrl: 'modules/georeferences/views/view-georeference.client.view.html'
      })
      .state('georeferences.edit', {
        url: '/:georeferenceId/edit',
        templateUrl: 'modules/georeferences/views/edit-georeference.client.view.html'
      });
  }
]);
