'use strict';

// Configuring the Articles module
angular.module('georeferences').run(['Menus',
  function (Menus) {
    // Add the articles dropdown item
    Menus.addMenuItem('topbar', {
      title: 'Georeferences',
      state: 'georeferences',
      type: 'dropdown'
    });

    // Add the dropdown list item
    Menus.addSubMenuItem('topbar', 'georeferences', {
      title: 'List georeferences',
      state: 'georeferences.list'
    });

    // Add the dropdown create item
    Menus.addSubMenuItem('topbar', 'georeferences', {
      title: 'Create georeferences',
      state: 'georeferences.create'
    });
  }
]);
