'use strict';

//Articles service used for communicating with the cities REST endpoints
angular.module('georeferences').factory('Georeferences', ['$resource',
  function ($resource) {
    return $resource('api/georeferences/:georeferenceId', {
      georeferenceId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);
