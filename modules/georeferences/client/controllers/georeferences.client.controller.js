'use strict';

// Articles controller
angular.module('georeferences').controller('GeoreferencesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Georeferences',
  function ($scope, $stateParams, $location, Authentication, Georeferences) {
    $scope.authentication = Authentication;

    // Create new City
    $scope.create = function () {
      // Create new Article object
      var georeference = new Georeferences({
        title: this.title,
        content: this.content
      });

      // Redirect after save
      georeference.$save(function (response) {
        $location.path('georeferences/' + response._id);

        // Clear form fields
        $scope.title = '';
        $scope.content = '';
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    // Remove existing Article
    $scope.remove = function (georeference) {
      if (georeference) {
        georeference.$remove();

        for (var i in $scope.georeferences) {
          if ($scope.georeferences[i] === georeference) {
            $scope.georeferences.splice(i, 1);
          }
        }
      } else {
        $scope.georeference.$remove(function () {
          $location.path('georeferences');
        });
      }
    };

    // Update existing Article
    $scope.update = function () {
      var georeference = $scope.georeference;

      georeference.$update(function () {
        $location.path('georeferences/' + georeference._id);
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    // Find a list of Articles
    $scope.find = function () {
      $scope.georeferences = Georeferences.query();
    };

    // Find existing Article
    $scope.findOne = function () {
      $scope.georeferences = Georeferences.get({
        georeferenceId: $stateParams.georeferenceId
      });
    };
  }
]);
