'use strict';

/**
 * Module dependencies.
 * es necesario teclear esto en mongo:  db.staffs.ensureIndex({coordinates:"2d"});
 * para que reconozca el campo como coordenadas para geolocalizacion
 */
var path = require('path'),
        mongoose = require('mongoose'),
        Client = mongoose.model('Client'),
        Staff = mongoose.model('Staff'),
        errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));


var getClientByUserId = function (userId, successCallback, errorCallback) {
    Client.find({user_account: userId}).populate('user', 'displayName').exec(function (err, client) {
        if (err) {
            errorCallback(err);
        } else if (!client) {
            errorCallback(err);
        }
        successCallback(client);
    });
};

var getNearStaff = function (lat, long, distance, successCallback, errorCallback) {
    
    Staff.find({coordinates: {$near: [lat, long], $maxDistance: distance / 111.12}},
    function (err, staffs) {
        if (err) {
            errorCallback(err);
        } else if (!staffs) {
            errorCallback(err);
        }
        successCallback(staffs);
    });
};

var searchConfig = function (config, param, successCallback, errorCallback) {
    var flag = false;
    config.forEach(function (c) {
        if (c.param === param) {
            successCallback(c);
            flag = true;
        }
    });
    if (!flag) {
        errorCallback('Config param not found...');
    }
};


/**
 * @api {get} /georeferences/neareststaffs Get nearest Staff around specified kilometers
 * @apiName GetGeorerenceNearestStaff
 * @apiGroup GeoReferences
 * @apiVersion 0.1.0
 * @apiPermission guest,admin
 * 
 * @apiSampleRequest http://localhost:3000/api/georeferences/neareststaffs
 * 
 * @apiSuccess (200) {String} name    names' new city
 * @apiSuccess (200) {Time}   state   states' new city
 * @apiSuccess (200) {Time}   country country's new city
 * @apiSuccess (200) {Object} user    user that created this document
 *    
 * @apiError (403) message         User is not authorized
 * @apiError (404) message         No staffs has been found
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *[
 * {
 *   "_id": "565a827b23434ae84887ff21",
 *   "user": "5653d7ba50eed9ae1bb874bd",
 *   "name": "Jose Alejandro Salvador Ruiz",
 *   "city": "5653e4fe32fda6be57eeb48d",
 *   "age": 26,
 *   "ranking": 4.5,
 *   "photo": "/foto.png",
 *   "latitude": 19.290609,
 *   "longitude": -99.040947,
 *   "active": true,
 *   "user_account": "565a820423434ae84887ff20",
 *   "__v": 0,
 *   "coordinates": [
 *     19.290609,
 *     -99.040947
 *   ],
 *   "hobbies": [
 *     "programar",
 *     "leer"
 *   ],
 *   "skills": [
 *     "correr",
 *     "jugar videojuegos"
 *   ],
 *   "phones": [
 *     "1234567890",
 *     "112233445566"
 *   ],
 *   "address": [
 *     "Direccion 1",
 *     "Direccion 2"
 *   ]
 * }
 *]
 *    
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 403 Forbidden
 *     {
 *       "message": "User is not authorized"
 *     }
 *     
 */
exports.neareststaffs = function (req, res) {
    //funcionalidad para geolocalizacion, ya paso por el middleware
    getClientByUserId(req.user.id, function (client) {
        searchConfig(client[0].config, 'distance', function (param) {
            getNearStaff(client[0].latitude, client[0].longitude, param.value, function (staff) {
                res.json(staff);
            }, function (err) {
                return res.status(404).send({
                    message: 'Dont get Staff near User'
                });
            });

        }, function (err) {
            console.log(err);
        });
    }, function (err) {
        return res.status(404).send({
            message: 'Not found Client'
        });
    });
};

