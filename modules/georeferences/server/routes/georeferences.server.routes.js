'use strict';

/**
 * Module dependencies.
 */
var georeferencesPolicy = require('../policies/georeferences.server.policy'),
    georeferences = require('../controllers/georeferences.server.controller');

module.exports = function (app) {
  // Single article routes
  app.route('/api/georeferences/neareststaffs').all(georeferencesPolicy.isAllowed)
    .get(georeferences.neareststaffs);

};
