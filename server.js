'use strict';
var exec = require('child_process').exec;
var cmd = 'npm run-script generate:api:doc';

exec(cmd, function(error, stdout, stderr) {
  console.log('generating ApiDoc document...');
  console.log(stdout);
});
/**
 * Module dependencies.
 */
var app = require('./config/lib/app');
var server = app.start();
